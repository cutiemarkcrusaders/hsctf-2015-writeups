# Sheep Enclosures - 400 points

Keith's longtime companion Daisy the Round and Fluffy Sheep needs your help!

[Problem Input](input.txt)

[Problem Description](description.txt)

This was broken but is now fixed.


## Solution

First of all, the area of the rubber band is simply the [convex hull](https://en.wikipedia.org/wiki/Convex_hull) of the set of all points of the fence. This can be calculated easily using algorithms available on the Internet. We used an implementation of the monotone chain algorithm, [available on Wikibooks](https://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain). The answer is simply the area between the convex hull and the fence.

Basically, we calculated the area of each of the polygons formed between the convex hull and the fence, although some of them degenerated to lines with area 0. First, we looped through the points on the fence, starting at a point on the convex hull. If a point was on the convex hull, we looked at all of the points on the fence between that point and the previous point that was on the convex hull. Then, we found the area of the polygon containing those points using the [shoelace algorithm](https://en.wikipedia.org/wiki/Shoelace_formula) and added it to the total.

The flag is **1389038164**.

[in/s/ane's solution](https://github.com/ChesleyTan/hsctf-in-s-ane/blob/master/Sheep-Enclosures_400/solve.py) was simpler than what we came up with: simply find the areas of both the convex hull and fence using the shoelace algorithm, and subtract.
