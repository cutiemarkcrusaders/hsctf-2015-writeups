#!/usr/bin/env python3

# Uses the monotone chain to find the convex hull of a set of points.
# Taken from http://en.wikibooks.org/wiki/Algorithm_Implementation/Geometry/Convex_hull/Monotone_chain
def convex_hull(points):
    """Computes the convex hull of a set of 2D points.

    Input: an iterable sequence of (x, y) pairs representing the points.
    Output: a list of vertices of the convex hull in counter-clockwise order,
      starting from the vertex with the lexicographically smallest coordinates.
    Implements Andrew's monotone chain algorithm. O(n log n) complexity.
    """

    # Sort the points lexicographically (tuples are compared lexicographically).
    # Remove duplicates to detect the case we have just one unique point.
    points = sorted(set(points))

    # Boring case: no points or a single point, possibly repeated multiple times.
    if len(points) <= 1:
        return points

    # 2D cross product of OA and OB vectors, i.e. z-component of their 3D cross product.
    # Returns a positive value, if OAB makes a counter-clockwise turn,
    # negative for clockwise turn, and zero if the points are collinear.
    def cross(o, a, b):
        return (a[0] - o[0]) * (b[1] - o[1]) - (a[1] - o[1]) * (b[0] - o[0])

    # Build lower hull
    lower = []
    for p in points:
        while len(lower) >= 2 and cross(lower[-2], lower[-1], p) <= 0:
            lower.pop()
        lower.append(p)

    # Build upper hull
    upper = []
    for p in reversed(points):
        while len(upper) >= 2 and cross(upper[-2], upper[-1], p) <= 0:
            upper.pop()
        upper.append(p)

    # Concatenation of the lower and upper hulls gives the convex hull.
    # Last point of each list is omitted because it is repeated at the beginning of the other list.
    return lower[:-1] + upper[:-1]


# Example: convex hull of a 10-by-10 grid.
assert convex_hull([(i//10, i%10) for i in range(100)]) == [(0, 0), (9, 0), (9, 9), (0, 9)]


# Uses the shoelace algorithm to find the area of a polygon.
# Taken from http://stackoverflow.com/a/24468019/
def PolygonArea(corners):
    n = len(corners) # of corners
    area = 0.0
    for i in range(n):
        j = (i + 1) % n
        area += corners[i][0] * corners[j][1]
        area -= corners[j][0] * corners[i][1]
    area = abs(area) / 2.0
    return area


def load_points():
    #return [(0, 0), (1, -1), (0, 1), (-1, -1)]
    with open('input.txt') as f:
        points = [tuple(map(int, line.split())) for line in f]
    return points


def main():
    points = load_points()
    hull_points = convex_hull(points)

    #print(hull_points)

    # Shift points array to start on the first point in the convex hull
    i = points.index(hull_points[0])
    points = points[i:] + points[:i]

    # Include the first point at the end as well
    points.append(points[0])

    last_i = -1
    total_area = 0
    for i, point in enumerate(points):
        if point in hull_points:
            corners = points[last_i:i+1]
            #print('corners', corners)

            area = PolygonArea(corners)
            print('from', last_i, points[last_i], 'to', i, point, 'area is', area)

            total_area += area
            last_i = i

    print('finished, last_i is {}, len(points) is {}'.format(last_i, len(points)))
    print('total_area is', total_area)

# This is in/s/ane's solution, which is more efficient
def main2():
    points = load_points()
    hull_points = convex_hull(points)

    fence_area = PolygonArea(points)
    hull_area = PolygonArea(hull_points)

    print('main2: fence_area = {}, hull_area = {}, difference = {}'.format(
        fence_area, hull_area, hull_area - fence_area))


if __name__ == '__main__':
    main()
    main2()
