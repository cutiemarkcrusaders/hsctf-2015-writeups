# Revenge of Cooley-Tukey - 300 Points

Keith found a file encrypt.txt on his computer which contains a series of numbers:

    1653.0 0.13916760914832338 58.72838425104149 -29.800988375644806 -64.39696961966996 77.24677622083472 29.83855931789015 -62.70175235211251 42.999999999999986 65.1382409674247 -21.49541356738255 -67.39554551772046 54.396969619669974 40.92032042100787 -39.07153000154913 -15.546218972937488 65.0

He also found a hint at the end of the file: Non-crypto related algorithm used for encryption

Something related to FFT. Is it DCT or DST? Don't know which one...

Help Keith find out what algorithm is used and decrypt the message to find the flag


## Solution

We were told that a [Fast Fourier Transform](https://en.wikipedia.org/wiki/Fast_Fourier_transform) was used to "encrypt" the message. To decrypt, simply take the inverse of both the Discrete Cosine Transform and Discrete Sine Transform (as we weren’t told which one was used). To get the flag, we round the resulting numbers and convert them into characters using their ASCII values.

We used the [Apache Commons Mathematics library](https://commons.apache.org/proper/commons-math/) to run the transformations because it supported various types of normalizations that other libraries, such as scipy, numpy, and JTransforms, did not. Note that this means that you must download the library to run the Java program.

The flag is **thegoodol'fourier**.
