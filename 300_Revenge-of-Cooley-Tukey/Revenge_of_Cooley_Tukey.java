import org.apache.commons.math3.exception.MathIllegalArgumentException;
import org.apache.commons.math3.transform.DctNormalization;
import org.apache.commons.math3.transform.DstNormalization;
import org.apache.commons.math3.transform.FastCosineTransformer;
import org.apache.commons.math3.transform.FastSineTransformer;
import org.apache.commons.math3.transform.TransformType;

public class Revenge_of_Cooley_Tukey
{
	public static void main(String[] args)
	{
		double[] numbers = {1653.0, 0.13916760914832338, 58.72838425104149, -29.800988375644806, -64.39696961966996, 77.24677622083472, 29.83855931789015, -62.70175235211251, 42.999999999999986, 65.1382409674247, -21.49541356738255, -67.39554551772046, 54.396969619669974, 40.92032042100787, -39.07153000154913, -15.546218972937488, 65.0};

		//Instantiate both a FastCosineTransformer and a FastSineTransformer
		FastCosineTransformer fct = new FastCosineTransformer(DctNormalization.STANDARD_DCT_I);
		FastSineTransformer fst = new FastSineTransformer(DstNormalization.STANDARD_DST_I);
		
		double[] decrypted = new double[numbers.length];
		try
		{
			System.out.println("> Trying cosine");
			decrypted = fct.transform(numbers, TransformType.INVERSE);
		}
		catch (MathIllegalArgumentException e)
		{
			System.out.println("> Using sine");
			decrypted = fst.transform(numbers, TransformType.INVERSE);
		}

		String s = "";
		for (int i = 0; i < decrypted.length; i++)
			s += (char) Math.round(decrypted[i]);
		
		System.out.println(s);
		//Flag is thegoodol'fourier
	}
}