# Befunge 2 - 200 Points

Keith now wants to test your capabilities with a simple (ish) program. Your program should print all powers of 2 from 2^1 to 2^15 (each power on a new line). Easy. The only problem is the character limit.

Max chars: 19

Grid: 4x15

You can access the Befunge2 runner using: nc 104.236.80.67 5678


## Solution

This is quite a step up from the previous Befunge problem. The output is 15 lines, and needs to print all the powers of 2 from 2 to 2^15, which is 32,768.

Clearly, we need to store two variables in the stack. The first variable is the current power of 2, and the second variable is the count (the exponent). Because there can only be 15 lines printed, the logic (in Python code) would look something like this:

	num = 1
	count = 15
	while count != 0:
		num *= 2
		print(num)
		count -= 1

The immediate problem is there are no variables in Befunge, just a stack that contains all the values. Thankfully, there is a Befunge operator, `\`, that swaps the top two values on the stack. This means that we can initialize our stack with `1 15`, and whenever we need to double the number we swap so the current power is on top, and whenever we need to decrement the counter we swap so the count is on the top.

Our code looked like this:

    >178+       v
    -1\.:+:\_@#:<

We start off by using `>` to start the program, and we then push 1, 7, and 8 onto the stack. The `+` operator adds the 7 and 8 and pushes 15. Following the arrows, we arrive at a `:` operator, which duplicates the value on the top of the stack.

This is important because once Befunge is done with a number, it discards it and does not push it back onto the stack. Thus, it is necessary to make another copy of any variables that you want to perform an operation on but still need in the future.

The `#` skips the `@` (which ends the program) and lands us on the `_`. What this does is it checks if the top of the stack is 0, and if it is, moves to the right, back to the `@` that we skipped. This will happen when the 15 that we started with gets decremented to 0, which will happen after 15 passes of the loop. If the top of the stack is not 0, it discards that value (which is why we duplicated it) and moves left.

The `\` swaps the two values on the stack, meaning that 1 is now on the top, followed by a 15. `:+:` duplicates, adds, and duplicates again, making our stack look like `2 2 15`. The `.` then pops and displays the top value.

Next, the top two values on the stack are swapped again, meaning that the count is now on the top. 1 is pushed and the two numbers are subtracted, which decreases count by 1. The stack now looks like `2 14`.

Here, it is important to remember that the array where the Befunge operators are stored is a torus, meaning that is wraps around. After the `-`, the program goes back to the `<` at the other end of the line, starting the loop over. After the next pass, 4 will be outputted and count will now equal 13. Finally, after all the powers of 2 from 2 to 32,768 are printed, count will be set to 0, which will make the `_` move the pointer right, ending the program.

Inputting this into the TCP service gets us the flag.

The flag is **2468...Oh wait, 24816...**
