# Befunge 3 - 300 Points

Keith has a new test for you. He wants you to make a new program which not only calculates any SINGLE power of two, but one that takes fewer characters than your previous creation! The input will be a single number 'n', and the output should be 2^n.

Max chars: 17

Grid: 5x15

You can access the Befunge3 runner using: nc 104.236.80.67 5679


## Solution

This problem is pretty much the same as Befunge 2, except that you only need to print out the last number in the loop and that instead of running it 15 times, it runs n times.

Our code looked like this, which was fairly similar to the code we wrote in Befunge 2.

    >1&>     :v
       ^-1\*2\_\.@

Key differences are that instead of `78+`, we simply used `&` to read the input and store it, and that instead of duplicating and adding, we just multiplied the top value by 2 and pushed it back in the stack. This was done as there was no need to print the value and thus no need to duplicate it in the stack.

After the loop has run n times, the `_` moves the pointer to the right, where it swaps the two values on the top of the stack, outputs 2^n, and ends the program.

Inputting this into the TCP service gets us the flag.

The flag is **2^n is so hard!**
