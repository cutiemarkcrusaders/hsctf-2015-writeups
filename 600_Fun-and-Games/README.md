# Fun and Games - 600 points

In the year 198 Keith decided to do too things, build a hunting range and open up a betting site! He filled the site with great game and made sure it was really fun. You can use it with http://104.236.80.67:5003. Can you win?

[Here's the source.](site.zip)

Clarification: This can be done in less than 100 requests, please don't DDOS the server with tons of requests.

## Solution

Evidently, we had to throw an exception in this block of code, while passing `bet > 10**15`, to get a flag.

    if not check_randomness(random_num):
        print "Redoing randomness for game number %d." % int(name[5:])
        random.seed()
    num = nums.pop()
    for i in range(100):
        if num == random_num():
            num = nums.pop()


### Exploiting random.randrange()

First, it looks like we have to make the randomness check fail. Let's look at the `random.randrange()` function ([source](https://github.com/python/cpython/blob/2.7/Lib/random.py#L175)). Although the function obtains random bytes if more than 53 bytes of randomness are requested (more than can fit into a double), the `_int(self.random() * width)` seems suspicious. Those of us who program in JavaScript have certainly been frustrated by having large integers rounded to nearby values.

Interestingly, the same code in Python 3.4 ([source](https://github.com/python/cpython/blob/3.4/Lib/random.py#L170)) is improved. Looking at the [blame](https://github.com/python/cpython/blob/3.4/Lib/random.py#L170) for the file, we find [*Issues #7889, #9025 and #9379: Improvements to the random module.*](https://github.com/python/cpython/commit/2f3cc4f7dac154832fce83762d6390b58e23b552), which appears to have fixed the problem with randrange. This leads us to [issue 9025](https://bugs.python.org/issue9025), which tells us that `random.randrange()` is far from uniform for large values.

The bug reporter, Mark Dickinson, notes in a comment that non-uniformity is clearly exhibited modulo 3 with the value `3/4 * 2**53`. So by guessing, if we want non-uniformity modulo 2, we should try `2/3 * 2**53 == 6004799503160661`, which miraculously, works. If we pass this as the bet, check_randomness will return False with high probability.


### Exploiting random.seed()

Upon seeing this problem, we were reminded of Guessing is Hard ([writeup](http://writeups.easyctf.com/content/200-guessing-is-hard.html)) in EasyCTF 2014, which was also written by Jacob Edelman. That problem asked us to attack Python's random number generator.

If we can predict the output of `random.randrange()`, which is completely dependent on its seed, then we would be able to predict the output of `random_num()`. The code would then run `num = nums.pop()`, which would throw an exception if the array was empty, giving the flag.

When `random.seed()` is called ([source](https://github.com/python/cpython/blob/2.7/Lib/random.py#L100)), Python tries to obtain 16 bytes from [`os.urandom`](https://docs.python.org/3/library/os.html#os.urandom), which uses the operating system's pseudo-RNG. However, if the system does not implement that capability, Python will throw a NotImplementedError. `random.seed()` will catch this and will instead seed its own RNG with the value of `long(time.time() * 256)`.

So we simply assumed that like in the EasyCTF problem, `os.urandom` was not available, and set about writing a Python script to find the answer. (Using sockets, for fun.) Note that the EasyCTF writeup fails to mention the offset between the server and local machine's time due to network latency and other factors, so that must be brute-forced.

However, this didn't work, no matter what we tried, so we went back to the block of code inside of which we had to raise an exception. It turns out that this code was a waste of time.


### The only (useful) exception

Finally, we went back to the block of code in `game_1` that would give us a flag if we threw an exception inside of it. Could we throw an exception anywhere else? Well, `random_num` could also throw an exception, but only if the bet was 0. All of this seemed strange, but the whole problem was poorly written Python code anyway, so it didn't rouse much suspicion.

Finally, we realized that `int(name[5:])` could also throw an exception. The value of `name` should be the name of the Python module that was running. When we sent a request to `/game_1?bet=0&nums=0`, the server would run `python game_1 0 0`, and the value of `name` would be `"game_1"`, so `name[5:]` would be `"1"`.

If we invoked the Python script some other way, `name[5:]` might not be an integer, and an exception would be thrown. For example, invoking the Python script directly would result in `name` being `__main__`, which would throw an exception. But we could not request `/game_1/__main__.py`, as dots were not allowed. Finally, we realized that requesting `/game_1/`, with a trailing slash, would produce the desired effect, because `"1/"` is not a valid integer. This allowed us to finally obtain the flag.

`Success! You bet 6004799503160661 which is more than one million so you win the flag. The flag is: wow_you_need_to_take_a_bit_of_time_away_from_the_computer_if_you_solved_this_you_nerd_:P`

The flag is **wow\_you\_need\_to\_take\_a\_bit\_of\_time\_away\_from\_the\_computer\_if\_you\_solved\_this\_you\_nerd\_:P**.
