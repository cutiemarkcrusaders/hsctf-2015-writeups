# HSCTF 2 Writeups

This repository contains writeups to some of the HSCTF 2 problems. All code and resources, unless stated otherwise in the repository, were written or created by the Cutie Mark Crusaders. However, everything is publicly available for anyone to use.

Note that several of the flags changed during the competition, sometimes many times. The ones that we list in our writeups are the ones that we entered.


## Links

Here are some links to other writeups that might also be useful.

* Jacob Edelman posted a gist, [Some 6 Word Write-Ups](https://gist.github.com/JacobEdelman/a811d86f63706e5744fd), consisting of a few short writeups that Jacob Edelman posted. Several others have also added their own short writeups in the comments.
* The in/s/ane team published their code for HSCTF 2 at [ChelseyTan / hsctf-in-s-ane](https://github.com/ChesleyTan/hsctf-in-s-ane). They do this for many of the CTFs that they do, and it's generally pretty helpful. Thanks, guys!
* ubuntor posted writeups for three problems at [ubuntor / hsctf-2-writeups](https://github.com/ubuntor/hsctf-2-writeups), including Keith the Xenolinguist.
* the2702.com has writeups for two problems: [Revenge of Cooley-Tukey](http://the2702.com/2015/05/15/Cooley-Tukey.html) and [Keith the Comedian](http://the2702.com/2015/05/16/Comedian.html).
* Duc Nguyen wrote a blog post, [Writeup HSCTF 2015](http://81glntd.blogspot.com/2015/05/wew-this-is-my-first-ctfwriteup-and.html), that explains several of the simpler binary problems.
