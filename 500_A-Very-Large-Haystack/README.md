# A Very Large Haystack - 500 points

Keith has found a very large [haystack](text.txt), which he knows contains a flag. Help him locate the flag in this haystack!


## Solution

It was clear that we needed to find the flag, which was mostly likely going to be a reasonably long phrase in English, in the file given. To search for English strings of text, we used a [Java program](Needle_Finder.java) that we wrote, along with a 10k word list that was found [here](https://raw.githubusercontent.com/first20hours/google-10000-english/master/google-10000-english.txt).

When first searching for strings of English words, we found the message `thesecretwordscanbeobtainedbyreadingeveryothercharacter`. After adding a split method to the Java file, searching through the two split strings yields `addtheseletterstotheotherhalfofthemessage`. We then wrote an add method in the Java file.

Letters were added using the following scheme:

    a + a = 0 + 0 = 0 = a
    a + b = 0 + 1 = 1 = b
    t + q = 19 + 16 = 35 = 9 (mod 26) = j

Searching through the new string got us `tofindthekeynowdothepreviousstepsanothertime`, and after splitting, adding, and searching again we found `nowdoallthatonemoretimetogettheanswer`. Doing those steps a fourth time gets the flag.

The output for the Java program can be viewed [here](output.txt).

The flag is **thesecharactersarethegatewaytokinglyglory**