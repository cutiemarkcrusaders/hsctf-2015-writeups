import java.io.File;
import java.io.FileNotFoundException;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.Scanner;

public class Needle_Finder // just bring a magnet
{
	public static void main(String[] args) throws FileNotFoundException
	{
		int blockSize = 100;
		int minCount = 10;

		// Load the haystack file
		Scanner haystackFile = new Scanner(new File("text.txt"));
		String haystack = haystackFile.nextLine().toLowerCase();
		haystackFile.close();

		// Load a list of common words
		// Source: https://github.com/first20hours/google-10000-english
		Scanner wordsFile = new Scanner(new File("10k.txt"));
		ArrayList<String> words = new ArrayList<String>(3000);

		while (wordsFile.hasNextLine() && words.size() < 3000) {
			String word = wordsFile.nextLine().toLowerCase();
			// Don't include words that are likely to randomly appear (< 3 chars)
			if (word.length() > 2) {
				words.add(word);
			}
		}
		wordsFile.close();


		String[] split;
		for (int i = 0; i < 4; i++) {
			System.out.println("Iteration " + i + ": ");

			// Search the haystack
			searchHaystack(haystack, blockSize, minCount, words);

			// Split and add
			split = splitEveryOther(haystack);
			haystack = addStrings(split[0], split[1]);
		}
	}

	public static ArrayList<String> searchHaystack(String haystack, int blockSize, int minCount, Iterable<String> words)
	{
		ArrayList<String> candidateBlocks = new ArrayList<String>();
		int len = haystack.length();

		for (int pos = 0; pos < len; pos += blockSize / 2) {
			String block = haystack.substring(pos, Math.min(pos + blockSize, len));
			int count = 0;

			for (String word : words) {
				if (block.contains(word))
					count++;
				
				//This counts how many times each word appears, but it's too slow
				/*int index = 0;
				while ((index = block.indexOf(word, index)) > -1) {
					count++;
				}*/
			}

			// Print the block if a sufficient number of words appear in it.
			if (count >= minCount) {
				System.out.println("Block found at pos = " + pos + " (count " + count + "): " + block);
				candidateBlocks.add(block);
			}
		}
		return candidateBlocks;
	}


	// Splits one string into two by taking every other character
	public static String[] splitEveryOther(String s)
	{
		StringBuilder s1 = new StringBuilder(s.length() / 2);
		StringBuilder s2 = new StringBuilder(s.length() / 2);
		for (int pos = 0; pos < s.length() - 1; pos += 2) {
			s1.append(s.charAt(pos));
			s2.append(s.charAt(pos + 1));
		}
		String[] split = {s1.toString(), s2.toString()};
		return split;
	}

	// Adds the values of two strings together, mod 26, using A=0, B=1, ... Z=25
	public static String addStrings(String s1, String s2)
	{
		String alphabet = "abcdefghijklmnopqrstuvwxyz";

		StringBuilder r = new StringBuilder();
		CharacterIterator it = new StringCharacterIterator(s1);
		CharacterIterator it2 = new StringCharacterIterator(s2);

		char c = it.first(), c2 = it2.first();
		while (c != CharacterIterator.DONE && c2 != CharacterIterator.DONE) {
			r.append(alphabet.charAt((alphabet.indexOf(c) + alphabet.indexOf(c2)) % 26));
			c = it.next(); c2 = it2.next();
		}
		return r.toString();
	}
}
