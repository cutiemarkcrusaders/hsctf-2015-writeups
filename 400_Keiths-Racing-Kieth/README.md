# Keith's Racing Kieth - 400 points

UPDATE: We are looking for the minimum number of points, not maximum. Sorry for any inconvenience.

Keith is racing his little brother Kieth!

[Problem Descrption](description.txt)

[Problem Input](input.txt)


## Solution

This is a fairly standard Dijkstra problem. The only difference is that instead there are two methods for weighting edges. The first is the time it takes to travel along one of the paths, which should be reduced. However, if the time it takes to travel along two paths is equal, then choose the path that gives the least number of points.

We wrote this in Java, so we simply updated the PriorityQueue's comparator and the relaxing code to reflect the different comparison method. In Python, normally a comparator isn't used, but you could just put tuples of `(time, points, to, from)` into a PriorityQueue and it'd sort automatically by first time then points.

Note that the provided Java program reads from standard input, so it has to be run redirecting the input from the terminal; for example, `java SpaceVacationer < input.txt`.

The flag is the minimum time and number of points that Keith can attain: **80 -4722**.
