import java.util.*;

public class Racing {
    static int V;
    static int E;
    static ArrayList<ArrayList<Edge>> graph;

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        V = sc.nextInt();
        E = sc.nextInt();

        graph = new ArrayList<ArrayList<Edge>>(V);
        for (int i = 0; i < V; i++) {
            graph.add(new ArrayList<Edge>());
        }

        for (int i = 0; i < E; i++) {
            int from = sc.nextInt(), to = sc.nextInt();
            int time = sc.nextInt(), points = sc.nextInt();
            Edge e1 = new Edge(from, to, time, points);
            graph.get(from).add(e1);
            Edge e2 = new Edge(to, from, time, points);
            graph.get(to).add(e2);
        }

        // yay dijkstra!!!
        dijkstra(0, V-1);
    }

    static void dijkstra(int start, int end) {
        final int[] dists = new int[V];
        final int[] points = new int[V];
        final int[] prev = new int[V];

        for (int i = 0; i < V; i++) {
            dists[i] = points[i] = Integer.MAX_VALUE / 2;
            prev[i] = -1;
        }
        dists[start] = points[start] = 0;

        PriorityQueue<Integer> pq;
        pq = new PriorityQueue<Integer>(E, new Comparator<Integer>() {
            @Override public int compare(Integer a, Integer b) {
                int cmp = dists[a] - dists[b];
                if (cmp != 0) return cmp;
                cmp = points[a] - points[b];
                if (cmp != 0) return cmp;
                return (Math.random() < 0.5 ? -1 : 1); // wait
            }
        });
        pq.add(start);

        while (!pq.isEmpty()) {
            int vertex = pq.remove();

            for (Edge edge : graph.get(vertex)) {
                int nextVertex = edge.to;
                // if shorter distance
                // OR if equal distance, less points
                if (dists[vertex] + edge.time < dists[nextVertex] ||
                        (dists[vertex] + edge.time == dists[nextVertex] &&
                         points[vertex] + edge.points < points[nextVertex])) {
                    dists[nextVertex] = dists[vertex] + edge.time;
                    points[nextVertex] = points[vertex] + edge.points;
                    prev[nextVertex] = vertex;
                    pq.add(nextVertex);
                }
            }
        }

        // print path
        int temp = end;
        while (temp != start) {
            int tempPrev = prev[temp];
            Edge edge = null;
            for (Edge e : graph.get(tempPrev)) {
                if (e.to == temp) { edge = e; break; }
            }
            System.out.println(tempPrev + " => " + temp +
                " (" + edge.time + ", " + edge.points + ")");
            temp = tempPrev;
        }

        System.out.println("result: " + dists[end] + " " + points[end] + " (= min time, points)");
    }

    static class Edge {
        int from, to, time, points;
        public Edge(int a, int b, int c, int d) {
            from = a; to = b; time = c; points = d;
        }
    }
}
