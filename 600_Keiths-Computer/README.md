# Keith's Computer - 600 points


Keith needs to leave his computer with his assistants as he goes to take over the world. Help him find a way to do this!

[Problem Description](description.txt)

[Problem Input](input.txt)


## Solution

Count the number of untrustworthy groups, after eliminating subsets of other groups.


## Proof, sort of

This is hardly a rigorous proof, and could use some cleanup.

First, the term "group" refers to a set of assistants. This section often refers to sets of groups, which are really sets of sets of assistants. It also refers to groups as subsets or supersets. Don't get confused!

Let's call the set described in the solution the set of "maximum untrustworthy" groups. No group in this set is a subset of another. We see that:

1. Groups that are **subsets** of any "maximum untrustworthy" group -> **untrustworthy** groups. (Removing assistants from a "maximum untrustworthy" group cannot make it trustworthy.)

2. Groups that are **strict supersets** of any "maximum untrustworthy" group -> **trustworthy** groups. (If a group in this set had a superset that was not trustworthy, it would be a subset of another element in the set, which means it shouldn't be in the set in the first place.)

3. If (1) and (2) are true, since untrustworthy/trustworthy and subsets/strict supersets are mutually exclusive and complete, then we can change the -> to a =, showing equivalence between the sets of groups. Maybe.

We propose a distribution of passwords that ensures that (1) and (2) are true. **For each "maximum untrustworthy" group *g*, let's create a password *P_g* given to every assistant that is *not* in *g*.**

As each "maximum untrustworthy" group *g* is missing a password *P_g*, it does not know all of the passwords. As a result, no subset of *g* knows all of the passwords, either. This is the same as all untrustworthy groups, so this satisfies (1).

However, each group *g* does know every password other than *P_g*. (For the sake of contradiction, assume that *g* does not know *P_x*, where x != g. Then *x* must be a superset of *g*. This is a contradiction, because then *g* would not be a "maximum untrustworthy" group.)

To obtain a trustworthy group, we can take a "maximum untrustworthy" group and add one or more assistants that are outside of the group. These members will know *P_g*, and as a result, the new group will know all of the passwords. This can be done to produce any trustworthy group, so this satisfies (2).

As a result, **one possible value for the number of keys required is equal to the size of the set of "maximum untrustworthy" groups.** But is this the minimum possible value? We don't know, but it gives us the flag, which is good enough.


## Example

First consider the sample input. The trustworthy groups are:

    {1, 2, 3, 4} {1, 2, 3} {1, 2, 4} {1, 3, 4} {2, 3, 4} {1, 2} {1, 3} {1, 4}

Then, we can find all untrustworthy groups by subtracting these groups from the set of all possible groups. (Note that there are 16 possible groups. Each assistant is either in or out of the set (2 possibilities), and 2**4 = 16.)

	{} {1} {2} {3} {4} {2, 3} {2, 4} {3, 4}

We eliminate `{} {2} {3} {4}` because they are subsets of other groups. Then, the maximum untrustworthy groups are:

	{1} {2, 3} {2, 4} {3, 4}

Finally, we assign passwords by taking the inverses of the above sets:

	{1}    -> Password 1: {2, 3, 4}
	{2, 3} -> Password 2: {1, 4}
	{2, 4} -> Password 3: {1, 3}
	{3, 4} -> Password 4: {1, 2}

This arrangement is equivalent to that specified by the problem description.


## Implementation

The solution code was fairly simple. First, we parse the set of all trustworthy groups `T`. To obtain the set of all untrustworthy groups `Tnot_set`, we subtract those from the set of all possible groups, `total_set`. The set of all possible groups is simply all subsets of the set {1, 2, 3, ..., N}.

Then, we used the Python code at http://stackoverflow.com/a/14107790/ (slightly modified) to eliminate subsets, producing `Tnot_max`. The size of this is the answer. Our code takes about 6 minutes to produce the answer on a relatively slow computer.

The flag is **16650**.
