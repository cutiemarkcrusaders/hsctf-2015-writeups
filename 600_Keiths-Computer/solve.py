#!/usr/bin/env python3

import operator, time
from collections import defaultdict
from functools import reduce
from itertools import chain, combinations

# This methods is based on http://stackoverflow.com/a/14107790/
def eliminate_subsets(sequence_of_sets):
    """Return a list of the elements of `sequence_of_sets`, removing all
    elements that are subsets of other elements.  Assumes that each
    element is a set or frozenset and that no element is repeated."""

    def is_power_of_two(n):
        """Returns True iff n is a power of two.  Assumes n > 0."""
        return (n & (n - 1)) == 0

    # The code below does not handle the case of a sequence containing
    # only the empty set, so let's just handle all easy cases now.
    if len(sequence_of_sets) <= 1:
        return list(sequence_of_sets)

    # We need an indexable sequence so that we can use a bitmap to
    # represent each set.
    sequence_of_sets = list(sequence_of_sets)

    # For each element, construct the list of all sets containing that
    # element.
    sets_containing_element = defaultdict(int)
    for i, s in enumerate(sequence_of_sets):
        for element in s:
            sets_containing_element[element] |= 1 << i
    print('eliminate_subsets: created sets_containing_element')

    # For each set, if the intersection of all of the lists in which it is
    # contained has length != 1, this set can be eliminated.
    def keep_set(s):
        intersection = reduce(operator.and_, (sets_containing_element[x] for x in s), -1)
        return is_power_of_two(intersection)

    out = list(filter(keep_set, sequence_of_sets))
    print('eliminate_subsets: filtered out subsets')

    return out


# This function is taken from https://docs.python.org/3/library/itertools.html
def powerset(iterable):
    "powerset([1,2,3]) --> () (1,) (2,) (3,) (1,2) (1,3) (2,3) (1,2,3)"
    s = list(iterable)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))


def load_data():
    with open('input.txt') as f:
        text = f.read().splitlines()

    N, M = map(int, text[0].split())
    T = [tuple(sorted(map(int, line.split()[1:]))) for line in text[1:]]

    return N, M, T


def main():
    start_time = time.clock()

    N, M, T = load_data()
    T_set = set(T)
    print('parsed input, N = {}, M = {}, |T| = {}'.format(N, M, len(T)))

    # Find Tnot (all untrustworthy groups)
    total_set = set(powerset(range(1, N+1)))
    Tnot_set = total_set - T_set
    Tnot = list(Tnot_set)
    print('found Tnot, |Tnot| =', len(Tnot))

    assert len(T) + len(Tnot) == len(total_set) == 2**N

    # Eliminate subsets to get Tnot_max
    Tnot_max = eliminate_subsets(Tnot)
    X = len(Tnot_max)
    print('eliminated subsets, X = |Tnot_max| =', X)
    print('time elasped: {:4f} s'.format(time.clock() - start_time))

    # The following code has never been run to completion, but the flag works anyway.
    """
    # Assign passwords to assistants
    # For each group, each assistant not in that group gets a password
    passwords_arr = [[] for n in range(N+1)] # ignore index 0
    for i, group in enumerate(Tnot_max):
        for n in range(1, N+1):
            if n not in group:
                passwords_arr[n].append(i)
    print('lengths of elements of passwords_arr:', list(map(len, passwords_arr)))

    # Check if the passwords work
    # A group should have all X passwords iff it is trustworthy
    for group in T_set:
        group_passwords = reduce(operator.or_, (set(passwords_arr[n]) for n in group))
        assert len(group_passwords) == X, (group, group_passwords)
    print('finished verifying passwords for T_set')

    for group in Tnot_set:
        group_passwords = reduce(operator.or_, (set(passwords_arr[n]) for n in group))
        assert len(group_passwords) < X, (group, group_passwords)
    print('finished verifying passwords for Tnot_set')

    print('time elasped: {:4f} s'.format(time.clock() - start_time))
    """


if __name__ == '__main__':
    main()

