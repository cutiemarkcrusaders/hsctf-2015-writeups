# Keith the Xenolinguist - 600 points

Keith has come across this really weird alien language. You can see his sample at xenolinguist.txt. Can you figure out what should replace the question marks in this final sample?

nhrpjahgpbjg ngjgagaaghpa kbbbpgarhgpj hbrbraagbrbg krrhprrrjggg ngahhghpjrpr halgemijig flordoralscorp flordoralscorp phphrapbgbhj fwooosh hpprgajgpaga halgemijig ghbpgrajjbj hapaaphbprgp halgemijig flordoralscorp kppbbaaabpja halgemijig hghrjgppgab fwooosh npgahhbghrb halgemijig ghagghjgjgga hrbapprrhgpj kjbjgggrabhp clogenheim ppbrbjhrjapr fwooosh bbgragapagja clogenheim halgemijig sgaprbgjpjh clogenheim fwooosh bpjhabbhapbb fwooosh flordoralscorp flordoralscorp thbjarpgagg halgemijig halgemijig clogenheim fwooosh fwooosh halgemijig clogenheim nbrjrrhrhjba halgemijig flordoralscorp flordoralscorp flordoralscorp npggghgagpgj fwooosh flordoralscorp halgemijig clogenheim flordoralscorp clogenheim rpgghjgppjjh halgemijig raphppbhhabj fwooosh ohrarajpabp halgemijig flordoralscorp fwooosh fwooosh aarhghppjjpg kgbbhajhjph clogenheim fwooosh ppjbjhhjbhjh fwooosh clogenheim halgemijig halgemijig halgemijig phpabagparbr halgemijig rpprrpjphjhj flordoralscorp rarjargarja halgemijig fwooosh kjbpgrahhhja fwooosh clogenheim clogenheim flordoralscorp halgemijig clogenheim clogenheim clogenheim halgemijig fwooosh fwooosh fwooosh orkshnoddle ???

Clarification: the ??? represents one word, not 3 characters.


## Solution

We were not able to solve this problem.

However, [ubuntor has written a writeup](https://github.com/ubuntor/hsctf-2-writeups/blob/master/KeithTheXenolinguist/README.md) for this problem.
