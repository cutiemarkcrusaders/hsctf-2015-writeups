# Keith the HSCTF Site Maker - 500 Points

Keith has decided to help out with HSCTF. His single contribution was to secretly make one change to the website in which a flag can be found. Find it!

## Solution

We started by using wget to download the entire website, and used grep to search for strings that might be in the flag. We also used a subdomain searcher and downloaded other sections of the website, but were unable to find anything there.

Luckily for us, one of our teammates was reading the About section and noticed that the HSCTF logo ([logo-mod.jpg](logo-mod.jpg)) seemed to contain a QR in it. (In addition, the URL of said logo was http://compete.hsctf.com/problem-static/forensics/keith_the_hsctf_site_maker/logo.jpg, making it pretty obvious that we were on the right track.) Everything needed to solve this problem was evidently in this file.


### Faint background (first two pieces)

To extract the QR code hidden in the background, we had to subtract the logo from the original image. We found that original image [on hscs.io](http://www.hscs.io/images/hsctf-icon.png) ([logo.png](logo.png)). Using a JavaScript program, we produced diffs of the two images; for example, let the output pixel be black if the two logos differ by more than 1 in RGB value.

Interestingly, we got different results for each color channel ([qr_r.png](qr_r.png), [qr_g.png](qr_g.png), [qr_b.png](qr_b.png)). The red channel provided the first piece of the QR code, which was visible in the bottom half of the image, and the green piece the second, stuck in the top-left corner.

By looking at the patterns in the pieces, we decided that the pieces were probably just stacked on top of each other for a height of 37, forming a version 5 QR code. This gave us the "left" half of the QR code, as we saw it then.


### In the file (third piece)

We used binwalk to find and extract the third piece of the QR code ([logoextract.jpg](logoextract.jpg)), which happened to be hidden inside of the logo-mod.jpg file. It seems that we had missed this on our initial inspection.

Because this piece appeared to be the top left, we had to think about what we should do with it. It turns out that this piece fit perfectly into the "top-right" of the QR code, as we saw it then, so we flipped it horizontally and placed it there. Now we were simply missing one final piece.


### Tupper (fourth piece)

At the end of logo-mod.jpg, there is "tupper", followed by a long string of digits. We weren't sure if it actually was supposed to read "upper", but eventually we assumed that it was a reference to [Tupper's self-referential formula](http://en.wikipedia.org/wiki/Tupper%27s_self-referential_formula). We concluded that we were supposed to substitute Tupper's number with the number we found. However, we were stuck for a while here.

Unfortunately, and irritatingly, it turns out that the correct equation to use is not actually the one Tupper proposed. Instead, it is one we found on [a LiveJournal blog](https://arvindn.livejournal.com/132943.html). Here is the formula they used:

    y >> (hx + y%h) & 1

And here is the formula that Tupper proposed, as a Python expression:

    0 < ((y // H) // (2**(H*x + y%H))) % 2

It turns out that the only difference between these two formulas is that the `(y // H)` term is replaced with simply `y`. Then, setting `H` to 45 and the width to something sufficiently large, we found the fourth piece of the QR code ([tupper_piece.png](tupper_piece.png)).


### Piecing it together

After piecing the first three pieces together, we realized that the info string was invalid, meaning that the QR had to be flipped along the top-left to bottom-right diagonal. That alignment gave us a valid info string, with a mask pattern of 4 (not important).

When we got the fourth piece of the QR, it fit into the remaining space of the QR code we had thus far fit together, without needing to flip it. This suggested that our previous step was correct.

(Here's a recap of the transformations we performed. First, we put the first piece in the top left of a 37x37 matrix and the second piece in the bottom left. Then we took the third piece, flipped it horizontally, and placed it into the top right. Then we flipped the whole thing on the diagonal running from the top left to the bottom right. Finally, we put the fourth piece in the bottom right.)

During this whole process, we transcribed all of the pieces by hand, using spaces and 1s to represent black and white squares. [drawqr.html](drawqr.html) can convert that into a scannable QR code. When we entered our final data, we obtained [qr_pieced_full.png](qr_pieced_full.png). Scanning the QR gives the flag.

The flag is **yes_keith_infiltrated_hsctf_only_to_change_the_logo_on_us_cause_it_made_a_great_problem_for_you**
