# A Strange Cult - 200 Points

Keith has discovered a strange cult based around a log, of all things! He needs you to answer this question based on its teachings:

If I create 13 holes in a log by replacing with it, what must I plant?


## Solution

Although this was meant to be obscure, one of our team members knew about this. Somehow.

Information on this cult can be found [here](https://www.fanfiction.net/u/1983129/Third-Fang). They state that for every hole in the log that was caused by your replacement, 10 saplings must be planted. Because 13 holes were created, 13 * 10 = 130 saplings must be planted.

The flag is **130 saplings**