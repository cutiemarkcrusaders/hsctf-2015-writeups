# Keith the Comedian - 400 Points

During the 2050s comedy got really really weird. Keith decided to enter into the surrealist comedy scene with his own joke. As was the norm for the time Keith made a 'Knock, knock!' joke but encoded it. We have no idea how he encrypted it but we think the joke has something to do with some sort of Hill. Here's the encrypted joke:

    1 232 92 231 59 226 115 31 51 68 53 51 147 218 52 234 17 234 54 144 241 13 49 149 155 247 168 88 22 177 20 238 205 3 86 155 182 240 18 211 121 35 215 3 123 110 108 157 109 229 21 9 166 254 119 238 173 44 142 121 3 86 155 95 42 227 107 102 67 35 193 187 118 112 207 87 254 87 113 131 54 71 243 7 180 100 84 108 100 25 199 5 103 15 170 114 159 203 136 132 155 43 83 66 237 217 2 159 126 60 27 101 181 69 142 45 199 242 180 99 111 158 219 6 174 230 91 127 4 61 151 158 180 100 84 135 222 232 223 81 56 85 63 57 246 92 119 237 196 252 158 143 105 150 131 173 183 226 102 141 240 199 63 82 233


## Solution

Using the hint in the problem text, we can infer that this is a [hill cipher](http://en.wikipedia.org/wiki/Hill_cipher). They also gave us the first few characters of the plaintext, which was "Knock, knock!". Thus, we know that we are using a known-plaintext attack to recover the full plaintext.

To encrypt a message using the Hill Cipher, the plaintext is first converted to a matrix. Another matrix, the key, is created. The key is then multiplied by the plaintext to get the cipher text.

    Key * Plaintext = Ciphertext

There are a few important things to keep in mind:

* Everything is mod 256, rather than mod 26
* Inverse is not a standard matrix inverse, but a modular matrix inverse. This means that the numbers in the inverse matrix will be less than 256, and they can be converted to their ASCII representations.
* When building the matrix, the characters start at the top left and go down before going across. Columns get filled before the rows.

The key for a hill cipher must be a square matrix, and because only 13 characters in the plaintext were given, the key must be 2x2 or 3x3. But the length of the ciphertext is 165, which is divisible by 3, but not by 2. So the key matrix, along with the known plaintext matrix, must be 3x3. This means that only the first 9 characters are used to recover the key.

Known plaintext matrix:

    K   o
    n   c   k
    o   ,   n

Ciphertext matrix:

      1   231   115    68   147  ...
    232    59    31    53   218  ...
     92   226    51    51    52  ...

It follows that:

    Key = First 3 columns of ciphertext * Modular inverse of plaintext
    Full plaintext = Modular inverse of key * Full ciphertext

The full plaintext turned out to be `Knock, knock! Who's there? An Ant Hill. I don't have an Aunt Hill! Hahahah (Humor of the 2050s was weird). Well anyway, the flag is: that_aunt_hill_joke_was_horrible`

The flag is **that_aunt_hill_joke_was_horrible**.

We found these two links very helpful while solving this problem:

* http://facultyfp.salisbury.edu/despickler/personal/Resources/LinearAlgebra/HillCipherHandoutLA.pdf
* http://www.nku.edu/~christensen/092mat483%20known%20plaintext%20attack%20of%20Hill%20cipher.pdf
