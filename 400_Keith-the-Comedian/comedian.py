#!/usr/bin/env python2

import numpy as np

#These three functions are used to calculate the modular inverse of a matrix
#Copied from https://github.com/ctfs/write-ups-2015/blob/master/ghost-in-the-shellcode-2015/crypto/nikoli/hilly.py

def modMatInv(A,p):       # Finds the inverse of matrix A mod p
	n=len(A)
	A=np.matrix(A)
	adj=np.zeros(shape=(n,n))
	for i in range(0,n):
		for j in range(0,n):
			adj[i][j]=((-1)**(i+j)*int(round(np.linalg.det(minor(A,j,i)))))%p
	return (modInv(int(round(np.linalg.det(A))),p)*adj)%p

def modInv(a,p):          # Finds the inverse of a mod p, if it exists
	for i in range(1,p):
		if (i*a)%p==1: return i
	raise ValueError(str(a)+" has no inverse mod "+str(p))

def minor(A,i,j):    # Return matrix A with the ith row and jth column deleted
	A=np.array(A)
	minor=np.zeros(shape=(len(A)-1,len(A)-1))
	p=0
	for s in range(0,len(minor)):
		if p==i: p=p+1
		q=0
		for t in range(0,len(minor)):
			if q==j: q=q+1
			minor[s][t]=A[p][q]
			q=q+1
		p=p+1
	return minor
    
mod = 256

#We know that the jokes starts with Knock, knock!
plaintext = "Knock, knock!"
plain = np.matrix([[ord(plaintext[0]), ord(plaintext[3]), ord(plaintext[6])],
                   [ord(plaintext[1]), ord(plaintext[4]), ord(plaintext[7])],
                   [ord(plaintext[2]), ord(plaintext[5]), ord(plaintext[8])]])

#This is the first 3 columns of the ciphertext
cipher = np.matrix("1, 231, 115; 232, 59, 31; 92, 226, 51")

#Ciphertext = Key * Plaintext
#Key = Ciphertext * Inverse of Plaintext
#All of this is in mod 256!
key = np.dot(cipher, modMatInv(plain, mod)) % mod
invkey = modMatInv(key, mod)

#Here is the full ciphertext
a1 = [1, 231, 115, 68, 147, 234, 54, 13, 155, 88, 20, 3, 182, 211, 215, 110, 109, 9, 119, 44, 3, 95, 107, 35, 118, 87, 113, 71, 180, 108, 199, 15, 159, 132, 83, 217, 126, 101, 142, 242, 111, 6, 91, 61, 180, 135, 223, 85, 246, 237, 158, 150, 183, 141, 63]
a2 = [232, 59, 31, 53, 218, 17, 144, 49, 247, 22, 238, 86, 240, 121, 3, 108, 229, 166, 238, 142, 86, 42, 102, 193, 112, 254, 131, 243, 100, 100, 5, 170, 203, 155, 66, 2, 60, 181, 45, 180, 158, 174, 127, 151, 100, 222, 81, 63, 92, 196, 143, 131, 226, 240, 82]
a3 = [92, 226, 51, 51, 52, 234, 241, 149, 168, 177, 205, 155, 18, 35, 123, 157, 21, 254, 173, 121, 155, 227, 67, 187, 207, 87, 54, 7, 84, 25, 103, 114, 136, 43, 237, 159, 27, 69, 199, 99, 219, 230, 4, 158, 84, 232, 56, 57, 119, 252, 105, 173, 102, 199, 233]
completecipher = np.matrix([a1, a2, a3])

#Now that we have the key,
#Plaintext = Inverse of key * Ciphertext
fullplain = np.dot(invkey, completecipher) % mod

m1 = fullplain[0]
m2 = fullplain[1]
m3 = fullplain[2]

deciphered = ""
for x in range(m1.shape[1]):
    deciphered += chr(int(m1.item(x))) + chr(int(m2.item(x))) + chr(int(m3.item(x)))

print(deciphered)

#Flag is that_aunt_hill_joke_was_horrible
