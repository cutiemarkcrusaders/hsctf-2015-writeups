# Befunge 4 - 400 Points

Keith has tried using Befunge himself. Its annoying though, because he comes form programming languages with more syntax and structures. He wants you to make one of the most important ones in Befunge. Make a program that simulates an integer array in Befunge.

Input:

1. p,val,pos (on seperate lines): puts the value 'val' in position 'pos'.
2. g,pos (on seperate lines): prints out the value at position 'pos'.
3. e: ends the program.
4. Note: The array have no more than 50 elements.

Max chars: 35

Grid: 10x50

You can access the Befunge4 runner using: nc 104.236.80.67 5680


## Solution

Befunge 1 to Befunge 4 was like going from Hello World to writing your own programming language.

A sample input string, as we discovered after someone clarified it on chat, would consist of some combinations of p’s and g’s and end with an e. p would be followed by 2 numbers, and g would be followed by 1. There would also be junk characters (not p, g, or e) between blocks of p’s, g’s, or the e to test that the program would correctly skip those characters.

For example,

    Input: p11p22p33p44p55p66p77JUNKCHARACTRSg1g2g3g4g5g6g7e
    Output: 1 2 3 4 5 6 7

Our code looked like this:

    v      _@
           ^   -2<
             >9-:|
    >782**~-:|   >&9g.
    ^    p9&&<

We first push the value of 112 onto the stack, which is the decimal representation of p. The input, which at this point should be a character, is also pushed onto the stack and the two numbers are subtracted. If the difference is 0, the two letters were the same, meaning that the input was p and was asking for the put command. Program execution then goes down a row and stores the next input, which is a number, at row 9 and the column indicated by the input after that.

If instead the difference was not 0, 9 is subtracted from the result of the first subtraction. If this is 0, then the input was a g because the decimal representation of g is 103, or 112 - 9. We then get the value at the 9th row and column specified by the input.

If that difference was not 0, we subtract 2, because 103 - 2 = 101, which is the decimal representation of e. If this difference is 0, we move to the right and end the program, if not, the input was a junk character and program execution goes back to the beginning, satisfying all requirements.

Inputting that into the TCP service gets us the flag.

The flag is **This took a long while**
