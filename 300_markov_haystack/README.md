# markov_haystack - 300 Points

Keith found this file on a flash drive and thinks there might be somthing in it, find it. [markovhaystack.txt](markovhaystack.txt)


## Solution

The haystack was generated using a Markov chain text generator from the Wikipedia article [Computer science](http://en.wikipedia.org/wiki/Computer_science). To solve the problem, we subtracted the set of all words in the Wikipedia article from the set of all words in the haystack. This was painful, because we couldn't figure out how the Wikipedia text was obtained, and we kept getting random garbage in the resulting set.

However, one of the resulting words in the output was "solution". `grep -i "solution" markovhaystack.txt` produces:

	Solution of this problem is "computer science logic" with no spaces.

The flag is **computersciencelogic**.

[in/s/ane's solution](https://github.com/ChesleyTan/hsctf-in-s-ane/blob/master/Markov-Haystack_300/solution.txt) to this problem was better. They simply used a frequency counter to determine which words appeared much less frequently than they should have, one of which was "solution".
