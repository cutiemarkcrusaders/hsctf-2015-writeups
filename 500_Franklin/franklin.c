// This is a rough approximation of the C code behind Franklin, decompiled by hand.
// Many things are probably wrong.

#include <stdio.h>

int grid[4][4];

int validate() {
    int arr[30]; // count of times each number appears in grid
    int c = 0;
    int b = 0;
    int i, j;

    for (i = 0; i <= 29; i++) {
        arr[i] = 0;
    }

    // ensure that all elements are 0 < x <= 30
    // then add to array as count
    for (i = 0; i <= 3; i++) {
        for (j = 0; j <= 3; j++) {
            if (grid[i][j] <= 0) {
                return 0;
            }
            if (grid[i][j] > 30) {
                return 0;
            }
            arr[grid[i][j] - 1]++;
        }
    }

    // make sure each count is <= 1
    for (i = 0; i <= 29; i++) {
        if (arr[i] > 1) {
            return 0;
        }
    }

    // c is sum of first row
    for (i = 0; i <= 3; i++) {
        c += grid[i][0];
    }

    // sum of each row must be equal to c
    for (i = 0; i <= 3; i++) {
        b = 0;
        for (j = 0; j <= 3; j++) {
            b += grid[i][j];
        }
        if (b != c) {
            return 0;
        }
    }


    // sum of each col must be equal to c
    for (i = 0; i <= 3; i++) {
        b = 0;
        for (j = 0; j <= 3; j++) {
            b += grid[j][i];
        }
        if (b != c) {
            return 0;
        }
    }


    // sum of diagonal from top-left to bototm-right must be equal to c
    b = 0;
    for (i = 0; i <= 3; i++) {
        b += grid[i][i];
    }
    if (b != c) {
        return 0;
    }


    // sum of diagonal from top-right to bottom-left must be equal to c
    b = 0;
    for (i = 0; i <= 3; i++) {
        b += grid[i][3-i];
    }
    if (b != c) {
        return 0;
    }


    int k; // probably should be declared at top or something

    // other diagonals must be equal to c
    for (i = 0; i <= 3; i++) {
        k = i;
        b = 0;
        for (j = 0; j <= 3; j++) {
            b += grid[i][k];
            k--;
            if (k < 0) {
                continue;
            }
        }
        if (b != c) {
            return 0;
        }
    }

    // INCOMPLETE
    for (i = 0; i <= 3; i++) {

    }
}

int main(int argc, *char argv[]) {
    int i, j;
    if (argc != 17) {
        puts("You need to input 16 numbers.");
        return 1;
    }
    for (i = 0; i <= 3; i++) {
        for (j = 0; j <= 3; j++) {
            grid[i][j] = strtol(argv[4*i+j+1], 0, 10);
        }
    }

    if (validate() == 0) {
        puts("No flag :(.");
    } else {
        give_flag();
    }
    return 0;
}
