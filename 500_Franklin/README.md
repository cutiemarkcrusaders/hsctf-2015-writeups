# Franklin - 500 points

No description needed. Here [ya](franklin) go. It runs on nc 104.236.80.67 5689 . The program asks for 16 integers. In the binary, these are inputted as arguments. In the server, you have to input these numbers semi-colon separated in standard in. For example, one possible input is `1;2;3;4;5;6;7;8;9;10;11;12;13;14;15;16`

## Solution
At first, we tried to decompile the file using some online decompilers, which proved to be not very fruitful, and in general a massive waste of time.

Eventually, we decided to get our hands dirty and start reading the assembly code ourselves and decompiling it by hand. The result is in `franklin.c`. However, we didn't finish decompiling the final for-loop. By the time we had gotten there, we realized that all of the previous for loops were merely to enforce the constraint that the numbers form a magic square (the sum of every row = the sum of every column = the sum of both diagonals). One of our teammates then started trying random magic squares, and surprise, one of them worked!

The correct input turned out to be: `22;1;20;7;19;8;21;2;5;18;3;24;4;23;6;17`

And the flag is **ben_franklin_was_mah_homie**.
