# A Mysterious Dashboard - 500 points

Update: The problem had an incorrect flag initially, but has been fixed.

Keith has discovered a mysterious dashboard and needs your help to figure out how it works!

[Problem Description](description.txt)

[Problem Input](input.txt)

## Solution

We were not able to solve this problem.
