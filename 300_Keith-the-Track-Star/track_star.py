#!/usr/bin/env python3

DARN = 2**32 # unreachable

def solve(N, K, hurdles):
    # The current row, with initial costs 0
    row = [0 for k in range(K)]

    for n in range(N):
        next_row = []

        for k in range(K):
            # If there's a hurdle at this position... darn.
            if (n, k) in hurdles:
                next_row.append(DARN)

            else:
                candidates = [
                    # It costs $0.00 to stay in your lane
                    row[k],

                    # The positions diagonally behind this one take 1 second
                    # If they're out of bounds... darn.
                    row[k-1] + 1 if k > 0   else DARN,
                    row[k+1] + 1 if k < K-1 else DARN,
                ]

                next_row.append(min(candidates))

        #print(next_row)
        row = next_row

    return row


def main():
    answers = []
    for i in range(1, 7+1):
        print('starting i =', i)
        with open('track_star_test{}.txt'.format(i)) as f:
            text = f.read().splitlines()

        N, K, H = map(int, text[0].split())
        hurdles = set( tuple(map(int, line.split()))
                       for line in text[1:] )
        assert H == len(hurdles)

        row = solve(N, K, hurdles)
        min_val = min(row)

        #print('final row:', row)
        print('min val:', min_val)

        answers.append(min_val)

    print('Flag:', ';'.join(map(str, answers)))


if __name__ == '__main__':
    main()
