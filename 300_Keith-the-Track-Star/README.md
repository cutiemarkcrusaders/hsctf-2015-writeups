# Keith the Track Star - 300 points

Even in the future, track is a very popular sport. Keith is trying it out, but is having some trouble. Here's the [description](description.txt). You are given 7 cases. The flag is the solution of each case in the order they appear here separted by semi-colons. For example, if there are 3 cases where the answer to case 1 is 5, the answer to case 2 is 8, and the answer case 3 is 1, the flag is 5;8;1 . The cases are as follows:

* [case1](track_star_test1.txt)
* [case2](track_star_test2.txt)
* [case3](track_star_test3.txt)
* [case4](track_star_test4.txt)
* [case5](track_star_test5.txt)
* [case6](track_star_test6.txt)


## Solution

(Note that [case 7](track_star_test7.txt) isn't linked from the problem statement. Oops! But it's not hard to guess its location. However, [in/s/ane's flag](https://github.com/ChesleyTan/hsctf-in-s-ane/blob/master/Keith-the-Track-Star_300/Solve.java) only consisted of the first six cases, so that might have been accepted as well.)

Interestingly, we can represent the track as a graph. Consider each position on the track, except for hurdles, to be a node. Each position has a directed edge to the position in front of it with weight 0, and to the weights diagonally in front of it with weight 1. If we construct this graph, with trivial modifications, we can perform Dijkstra's algorithm to obtain the result! However, that's probably overkill.

Instead, we can run a simple in-place DP algorithm. Let's start by creating an array that represents a row on the track. Its values are the shortest distances possible to each position. However, we mark hurdles with a distance of some large number DARN to indicate that they are unreachable.

        N
       /|\
      / | \
     /  |  \
    A   B   C

Then, we can calculate the next row. Let's consider each position (N). Firstly, if there is a hurdle at that position, we mark its value as DARN. Otherwise, its value is either the same as the value of the position behind it (B), or 1 more than the value of the positions diagonally behind it (A and C). We choose the minimum of the three values. (Note that because DARN is a large number, if A, B, or C is unreachable, their values will not be chosen unless they are all unreachable, in which case the current position is also unreachable.) The final answer for each of the input files is simply the smallest value in the final row.

If that wasn't very clear, just read the code! It's pretty easy to understand.

The flag is **2027;2009;2028;2014;2003;2010;2031**.
