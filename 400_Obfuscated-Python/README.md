# Obfuscated Python - 400 Points

Keith writes really really ugly python code. Can you figure out what the flag is?

[Keiths program](obfuscated_python.pyc).


## Solution

Initially, we tried to use several different Python decompilers, including [pycdc](https://github.com/zrax/pycdc), [uncompyle](https://github.com/gstarnberger/uncompyle), and [uncompyle2](https://github.com/gstarnberger/uncompyle). As intended by the problem writers, all of these failed in one way or another to produce any meaningful output.

Because of this, we resorted to disassembling. Fortunately, the `dis` module in Python was made to do this, and a blog post, [The structure of .pyc files](http://nedbatchelder.com/blog/200804/the_structure_of_pyc_files.html), contains a program that uses that module to disassemble a .pyc file. (Note that on line 7, we had to change `'L'` to `'<L'` .)

Using this module, we obtain the results in [obfuscated_python_info.txt](obfuscated_python_info.txt). However, the disassembly also segfaults after a while (sigh). So if we comment out the `dis.disassemble` line, we obtain the results in [obfuscated_python_info_no_dis.txt](obfuscated_python_info_no_dis.txt). This time, it doesn't throw any exception at all, but the output isn't really that useful.

Fortunately for us, the disassembly that is generated is extremely readable, though we added comments for extra clarity. Here's what the original file probably looked like:

    n = 6321187
    x = ""

    for i in open(__file__).read().split("lol")[1]:
        n **= 2
        n %= 256
        v = n ^ ord(i)
        v %= 256
        x += chr(v)

    exec x
    print 'Done'

Evidently, the program is reading its own file (`__file__`), then performing a transformation on the part that comes after `lol` and then executing it. (Wait, that's why it's corrupted, but Python can still run it.) To solve this, we save this as a script, change `__file__` to `obfuscated_python.pyc`, and change `exec x` to `print x`. This way, we'll print out the code that's executed, rather than actually executing it.

    import sys
    if raw_input('Enter password: ') == 'dassdjewufweubhbwefbiasysfdysfd': print 'The flag is: python_can_be_obfuscated_a_bit_i_guess'
    else: print 'Wrong password.'
    sys.exit(0)

The flag is **python_can_be_obfuscated_a_bit_i_guess**.

There are more than a few ways to solve this problem. [The solutions](https://github.com/ChesleyTan/hsctf-in-s-ane/blob/master/Obfuscated-Python_400/solution.txt) that in/s/ane and Jacob Edelman proposed were completely different from ours, and both are incredibly cool.
