# Keith the Space Vacationer - 400 points

In the past, there were vacations on Earth. Now, there are vacations in space. Help Keith go on his very first space vacation. Problem description is located [here](description.txt). There are six cases for this problem. The flag format is `case1_answer;case_2_answer;case3_answer;case4_answer;case5_answer;case6_answer`. Here are the cases:

* [case 1](vacationer_test1.txt)
* [case 2](vacationer_test2.txt)
* [case 3](vacationer_test3.txt)
* [case 4](vacationer_test4.txt)
* [case 5](vacationer_test5.txt)
* [case 6](vacationer_test6.txt)


## Solution

This problem is a slightly modified Dijkstra problem. First, compute the array of shortest times to each planet that can be obtained, ignoring excellent trains, using Dijkstra's algorithm. After this point, do not modify this first array. Then copy the first array into a second array that represents the shortest times that can be obtained using at most one excellent train. Add each planet that an excellent train departs from to a priority queue (as those edges need to be relaxed), then use this priority queue to run Dijkstra's algorithm again.

However, this time, don't ignore excellent trains when relaxing. Instead, when considering an excellent train, add its time to the shortest time to the start node without an excellent train (first array), then compare this to the shortest time to the end node with at most one excellent train (second array). When considering a regular train, perform relaxation with the second array. Look at the second array for the answer.

Note that the provided Java program reads from standard input, so it has to be run redirecting the input from the terminal; for example, `java SpaceVacationer < vacationer_test1.txt`.

The flag is the concatenation of the shortest times to the final planet for each input set: **34;15;26;40;30;31**.
