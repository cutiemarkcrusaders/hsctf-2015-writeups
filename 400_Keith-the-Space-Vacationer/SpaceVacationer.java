import java.io.*;
import java.util.*;

public class SpaceVacationer {
    static int V;
    static int E;
    static ArrayList<ArrayList<Edge>> edges;

    static double shortestPath(int src, int dest) {
        // initialize
        final int[] dists = new int[V];
        for (int i = 0; i < V; i++) {
            dists[i] = Integer.MAX_VALUE / 2;
        }

        PriorityQueue<Integer> pq = new PriorityQueue<Integer>(V, new Comparator<Integer>() {
            public int compare(Integer a, Integer b) {
                return dists[a] - dists[b];
            }
        });

        dists[src] = 0;
        pq.add(src);


        // Save nodes with excellent paths
        HashSet<Integer> excellentNodes = new HashSet<Integer>(V);

        // first, perform Dijkstra ignoring excellent paths
        while (!pq.isEmpty()) {
            int vertex = pq.remove();
            for (Edge edge : edges.get(vertex)) {
                if (edge.excellent) {
                    excellentNodes.add(vertex);
                    continue;
                }

                // relax edge to neighbor
                int neighbor = edge.to;
                if (dists[vertex] + edge.weight < dists[neighbor]) {
                    dists[neighbor] = dists[vertex] + edge.weight;
                    pq.add(neighbor);
                }
            }
        }

        //System.out.println("Dists ignoring excellent: " + Arrays.toString(dists));

        final int[] oldDists = Arrays.copyOf(dists, V);

        // add nodes with excellent paths
        for (Integer node : excellentNodes) {
            pq.add(node);
        }

        // dijkstra, with excellent paths
        while (!pq.isEmpty()) {
            int vertex = pq.remove();
            //System.out.println("removed " + vertex);
            for (Edge edge : edges.get(vertex)) {
                //System.out.println("checking edge " + edge);
                int distHere = dists[vertex];
                if (edge.excellent) {
                    distHere = oldDists[vertex];
                }

                // relax edge to neighbor
                int neighbor = edge.to;
                if (distHere + edge.weight < dists[neighbor]) {
                    //System.out.println("relaxing edge to " + neighbor + ": " + (distHere + edge.weight));
                    dists[neighbor] = distHere + edge.weight;
                    pq.add(neighbor);
                }
            }
        }

        //System.out.println("Dists with excellent: " + Arrays.toString(dists));

        return dists[dest];
    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        V = sc.nextInt();
        E = sc.nextInt();

        edges = new ArrayList<ArrayList<Edge>>(V);
        for (int i = 0; i < V; i++) {
            edges.add(new ArrayList<Edge>());
        }

        for (int i = 0; i < E; i++) {
            int from = sc.nextInt(),
                to = sc.nextInt(),
                weight = sc.nextInt(),
                excellent = sc.nextInt();
            Edge e = new Edge(from, to, weight, excellent != 0);
            edges.get(from).add(e);
        }

        double dist = shortestPath(0, V-1);
        System.out.println("Shortest path: " + dist);
    }

    static class Edge {
        int from;
        int to;
        int weight;
        boolean excellent;

        public Edge(int f, int t, int w, boolean e) {
            this.from = f;
            this.to = t;
            this.weight = w;
            this.excellent = e;
        }
        @Override
        public String toString() {
            return String.format("Edge[from=%d, to=%d, weight=%d, excellent=%b]",
                from, to, weight, excellent);
        }
    }
}
