# Befunge 1 - 100 Points

Keith has decided that he (or rather, you), will learn a strange programming language for him.
Here's a wikipedia article on it: http://en.wikipedia.org/wiki/Befunge
Keith's first test for you: make a program that will take two numbers as input (seperately), multiply them,
and then print out the product.

Max chars: 6

Grid: 1x10

You can access the Befunge1 runner using: nc 104.236.80.67 5677


## Solution

This problem was very straightforward. All that is required is a understanding of some of the Befunge operators.

The operations that were used can be found here:

    > Starts the program, must be included
    & Reads an integer (only 1 digit), and pushes it onto the stack
    * Pops the top two values, multiplies them, and pushes the result onto the stack
    . Pops value and outputs as an integer
    @ Ends the program, must be included

It is fairly easy to see what must be done. Simply start the program, read two integers, multiply them, output the result, and then end the program.

	>&&*.@

Inputting this into the TCP service gets us the flag.

The flag is **A Whole New Language?**
